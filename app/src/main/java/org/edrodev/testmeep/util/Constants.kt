package org.edrodev.testmeep.util

object Constants {

    const val LOWER_LEFT_LAT = 38.711046
    const val LOWER_LEFT_LON = -9.160096
    const val UPPER_RIGHT_LAT = 38.739429
    const val UPPER_RIGHT_LON = -9.137115

    const val INITIAL_LAT = (LOWER_LEFT_LAT + UPPER_RIGHT_LAT) / 2
    const val INITIAL_LON = (LOWER_LEFT_LON + UPPER_RIGHT_LON) / 2
    const val INITIAL_ZOOM = 13.5f

}