package org.edrodev.testmeep.util

import android.content.Context
import org.edrodev.testmeep.R

object ResourceTypeResolver {

    @JvmStatic
    fun getResourceName(context: Context, resourceType: String?) : String? =
            when(resourceType) {
                "MOPED" -> context.getString(R.string.resource_name_moped)
                "CAR" -> context.getString(R.string.resource_name_car)
                "ELECTRIC_CAR" -> context.getString(R.string.resource_name_electric_car)
                else -> null
            }
}