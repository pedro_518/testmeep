package org.edrodev.testmeep.di

import org.edrodev.testmeep.BuildConfig
import org.edrodev.testmeep.network.ApiUtils
import org.edrodev.testmeep.network.MeepApi
import org.edrodev.testmeep.network.MeepGsonManager
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val apiModule = module {

    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.HOST)
            .addConverterFactory(GsonConverterFactory.create(MeepGsonManager.gson))
            .build()
    }

    single { ApiUtils.createApi<MeepApi>(get()) }

}