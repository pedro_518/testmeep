package org.edrodev.testmeep.di

import org.edrodev.testmeep.repository.meeprepository.MeepRespository
import org.edrodev.testmeep.repository.meeprepository.impl.MeepRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {

    single<MeepRespository> {
        MeepRepositoryImpl(
            meepApi = get()
        )
    }

}