package org.edrodev.testmeep.di

import org.edrodev.testmeep.viewmodel.MeepMapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        MeepMapViewModel(
            meepRespository = get()
        )
    }

}