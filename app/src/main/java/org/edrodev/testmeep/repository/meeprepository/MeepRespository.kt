package org.edrodev.testmeep.repository.meeprepository

import arrow.core.Either
import org.edrodev.testmeep.model.Resource

interface MeepRespository {

    /**
     * Get the resources of a frame
     *
     * @param lowerLeftLat lower left latitude of the frame
     * @param lowerLeftLon lower left longitude of the frame
     * @param upperRightLat lower right latitude of the frame
     * @param upperRightLon lower tight longitude of the frame
     *
     * @return [Either] Left: [Throwable] Right: [List] of [Resource] of the frame
     */
    suspend fun getResources(
        lowerLeftLat: Double,
        lowerLeftLon: Double,
        upperRightLat: Double,
        upperRightLon: Double
    ) : Either<Throwable, List<Resource>>
}