package org.edrodev.testmeep.repository.meeprepository.impl

import arrow.core.Either
import arrow.core.Try
import org.edrodev.testmeep.model.Resource
import org.edrodev.testmeep.network.MeepApi
import org.edrodev.testmeep.repository.meeprepository.MeepRespository

class MeepRepositoryImpl(
    private val meepApi: MeepApi
) : MeepRespository {
    override suspend fun getResources(
        lowerLeftLat: Double,
        lowerLeftLon: Double,
        upperRightLat: Double,
        upperRightLon: Double
    ): Either<Throwable, List<Resource>> = Try {
        meepApi.getResources(
            lowerLeftLatLon = "$lowerLeftLat, $lowerLeftLon",
            upperRightLatLon = "$upperRightLat, $upperRightLon"
        )
    }.toEither()
}