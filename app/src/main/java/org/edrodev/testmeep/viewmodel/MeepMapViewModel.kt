package org.edrodev.testmeep.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.edrodev.testmeep.model.Resource
import org.edrodev.testmeep.repository.meeprepository.MeepRespository

class MeepMapViewModel(
    private val meepRespository: MeepRespository
) : ViewModel() {

    private val _resources = MutableLiveData<List<Resource>>()
    val resources: LiveData<List<Resource>> get() = _resources

    init {
//        findResources()
    }

    fun findResources(lowerLeftLat: Double, lowerLeftLon: Double, upperRightLat: Double, upperRightLon: Double) = viewModelScope.launch(Dispatchers.IO) {
        meepRespository.getResources(
            /*Constants.LOWER_LEFT_LAT,
            Constants.LOWER_LEFT_LON,
            Constants.UPPER_RIGHT_LAT,
            Constants.UPPER_RIGHT_LON*/
            lowerLeftLat,
            lowerLeftLon,
            upperRightLat,
            upperRightLon
        ).fold(
            ifRight = {resources ->

                // Muestro solo las que son nuevas
                val newResources = resources.filter { _resources.value?.contains(it) != true }

                withContext(Dispatchers.Main) { _resources.value = newResources }
            },

            ifLeft = {
                Log.e("findResources", it.toString())
            }
        )
    }

}