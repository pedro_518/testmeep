package org.edrodev.testmeep

import android.app.Application
import org.edrodev.testmeep.di.apiModule
import org.edrodev.testmeep.di.repositoryModule
import org.edrodev.testmeep.di.viewModelModule
import org.koin.core.context.startKoin

class MeepApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(
                listOf(
                    apiModule,
                    repositoryModule,
                    viewModelModule
                )
            )
        }

    }

}