package org.edrodev.testmeep.ui

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import org.edrodev.testmeep.databinding.MeepInfoWindowResourceLayoutBinding
import org.edrodev.testmeep.model.Resource
import org.edrodev.testmeep.util.GlideApp
import androidx.core.os.HandlerCompat.postDelayed




class MeepInfoWindowAdapter(context: Context) : GoogleMap.InfoWindowAdapter {

    private val inflater by lazy { LayoutInflater.from(context) }

    private var markerResourceMap: MutableMap<String, Resource> = mutableMapOf()

    fun addResourceInfo(marker: Marker, resource: Resource) {
        markerResourceMap[marker.id] = resource
    }

    override fun getInfoContents(marker: Marker): View? {
        val resource = markerResourceMap[marker.id]
        val binding = createBinding(resource)

        if(resource?.resourceImageId != null) {

            val imageUrl = binding.root.context.getString(
                org.edrodev.testmeep.R.string.resource_image_url,
                resource.resourceImageId
            )
            GlideApp.with(binding.root)
                .load(imageUrl)
                .addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        Handler().postDelayed({
                            if (marker.isInfoWindowShown) {
                                marker.showInfoWindow()
                            }
                        }, 100)
                        return false
                    }
                })
                .centerCrop()
                .into(binding.pic)
        }

        return binding.root
    }

    override fun getInfoWindow(marker: Marker): View? = null

    private fun createBinding(resource: Resource?) : MeepInfoWindowResourceLayoutBinding =
        MeepInfoWindowResourceLayoutBinding.inflate(inflater).apply {
            this.resource = resource
        }.apply {
            executePendingBindings()
        }

}