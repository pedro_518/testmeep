package org.edrodev.testmeep.network

import org.edrodev.testmeep.model.Resource
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MeepApi {

    @Headers("accept: application/json")
    @GET("tripplan/api/v1/routers/lisboa/resources")
    suspend fun getResources(
        @Query("lowerLeftLatLon") lowerLeftLatLon: String,
        @Query("upperRightLatLon") upperRightLatLon: String
    ) : List<Resource>
}