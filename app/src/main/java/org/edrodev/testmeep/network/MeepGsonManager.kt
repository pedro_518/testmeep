package org.edrodev.testmeep.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.edrodev.testmeep.model.Resource
import org.edrodev.testmeep.serialization.BooleanSerializer
import org.edrodev.testmeep.serialization.ResourceDeserializer

object MeepGsonManager  {

    val gson: Gson by lazy {

        GsonBuilder()
            .registerTypeAdapter(Boolean::class.java, BooleanSerializer())
            .registerTypeAdapter(Resource::class.java, ResourceDeserializer())
            .create()
    }
}