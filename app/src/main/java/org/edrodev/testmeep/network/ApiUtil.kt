package org.edrodev.testmeep.network

import retrofit2.Retrofit

object ApiUtils {

    inline fun <reified T> createApi(retrofit: Retrofit): T = retrofit.create(T::class.java)
}