package org.edrodev.testmeep


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.edrodev.testmeep.model.BikesResource
import org.edrodev.testmeep.model.CarResource
import org.edrodev.testmeep.model.EngineCarResource
import org.edrodev.testmeep.model.MopedResource
import org.edrodev.testmeep.ui.MeepInfoWindowAdapter
import org.edrodev.testmeep.util.Constants
import org.edrodev.testmeep.viewmodel.MeepMapViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MeepFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val meepMapViewModel: MeepMapViewModel by viewModel()

    private val infoWindowAdapter by lazy { MeepInfoWindowAdapter(context!!) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meep, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        meepMapViewModel.resources.observe(this, Observer {

            lifecycleScope.launch(Dispatchers.Default) {
                it.map { resource ->
                    val latLon = LatLng(resource.y, resource.x)
                    val color = when (resource) {
                        is BikesResource -> BitmapDescriptorFactory.HUE_YELLOW
                        is EngineCarResource -> BitmapDescriptorFactory.HUE_MAGENTA
                        is CarResource -> BitmapDescriptorFactory.HUE_BLUE
                        is MopedResource -> BitmapDescriptorFactory.HUE_GREEN
                        else -> BitmapDescriptorFactory.HUE_ORANGE
                    }

                    val markerOptions = MarkerOptions()
                        .position(latLon)
                        .icon(BitmapDescriptorFactory.defaultMarker(color))

                    withContext(Dispatchers.Main) {
                        val marker = mMap.addMarker(markerOptions)

                        infoWindowAdapter.addResourceInfo(marker, resource)
                    }

                }

            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setInfoWindowAdapter(infoWindowAdapter)

        mMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(Constants.INITIAL_LAT, Constants.INITIAL_LON),
                Constants.INITIAL_ZOOM
            )
        )

        mMap.setOnCameraIdleListener {
            val curScreen = googleMap.projection
                .visibleRegion.latLngBounds

            meepMapViewModel.findResources(
                curScreen.southwest.latitude,
                curScreen.southwest.longitude,
                curScreen.northeast.latitude,
                curScreen.northeast.longitude
            )
        }
    }


}
