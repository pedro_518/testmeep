package org.edrodev.testmeep.extensions

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import org.edrodev.testmeep.util.GlideApp

@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(image : String?) {

    GlideApp.with(this)
//        .load(image)
        .load("https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg")
        .fitCenter()
        .into(this)
}