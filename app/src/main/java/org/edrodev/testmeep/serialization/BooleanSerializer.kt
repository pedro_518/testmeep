package org.edrodev.testmeep.serialization

import com.google.gson.*
import java.lang.reflect.Type

class BooleanSerializer : JsonSerializer<Boolean> {

    override fun serialize(arg0: Boolean?, arg1: Type, arg2: JsonSerializationContext): JsonElement {
        return JsonPrimitive(java.lang.Boolean.TRUE == arg0)
    }
}