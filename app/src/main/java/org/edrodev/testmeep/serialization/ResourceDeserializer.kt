package org.edrodev.testmeep.serialization

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.edrodev.testmeep.extensions.fromJson
import org.edrodev.testmeep.model.*
import java.lang.reflect.Type

class ResourceDeserializer : JsonDeserializer<Resource> {

    private val gson = GsonBuilder()
        .registerTypeAdapter(Boolean::class.java, BooleanSerializer())
        .create()

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Resource {

        val jsonObject = json.asJsonObject
        return if (jsonObject.has("resourceType")) {
            when (jsonObject.get("resourceType").asString) {
                "MOPED" -> gson.fromJson<MopedResource>(jsonObject.toString())

                "CAR" -> gson.fromJson<EngineCarResource>(jsonObject.toString())

                "ELECTRIC_CAR" -> gson.fromJson<CarResource>(jsonObject.toString())

                else -> {
                    gson.fromJson<Resource>(jsonObject.toString())
                }
            }
        } else {

            if (jsonObject.has("bikesAvailable")) {
                gson.fromJson<BikesResource>(jsonObject.toString())
            } else
                gson.fromJson<Resource>(jsonObject.toString())
        }
    }

}