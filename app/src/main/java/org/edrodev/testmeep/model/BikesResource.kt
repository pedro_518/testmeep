package org.edrodev.testmeep.model

import com.google.gson.annotations.SerializedName

class BikesResource : Resource() {

    @SerializedName("station")
    var station: Boolean = false

    @SerializedName("bikesAvailable")
    var bikesAvailable: Int = 0

    @SerializedName("availableResources")
    var availableResources: Int = 0

    @SerializedName("spacesAvailable")
    var spacesAvailable: Int = 0

    @SerializedName("allowDropoff")
    var allowDropoff: Boolean = false
}