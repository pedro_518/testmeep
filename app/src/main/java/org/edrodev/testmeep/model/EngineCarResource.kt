package org.edrodev.testmeep.model

import com.google.gson.annotations.SerializedName

class EngineCarResource : CarResource() {

    @SerializedName("pricePerMinuteParking")
    var pricePerMinuteParking: Float = 0f

    @SerializedName("pricePerMinuteDriving")
    var pricePerMinuteDriving: Float = 0f

    @SerializedName("engineType")
    var engineType: String = ""
}