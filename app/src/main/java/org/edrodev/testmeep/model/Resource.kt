package org.edrodev.testmeep.model

import com.google.gson.annotations.SerializedName

open class Resource {

    @SerializedName("id")
    var id: String = ""

    @SerializedName("name")
    var name: String = ""

    @SerializedName("x")
    var x: Double = 0.0

    @SerializedName("y")
    var y: Double = 0.0

    @SerializedName("companyZoneId")
    var companyZoneId: Int = -1

    @SerializedName("lat")
    var lat: Double = 0.0

    @SerializedName("lon")
    var lon: Double = 0.0

    @SerializedName("realTimeData")
    var realTimeData: Boolean? = null

    @SerializedName("resourceImageId")
    var resourceImageId: String? = null

    @SerializedName("resourceType")
    var resourceType: String? = null
}