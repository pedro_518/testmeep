package org.edrodev.testmeep.model

import com.google.gson.annotations.SerializedName

open class CarResource : VehicleResource() {

    @SerializedName("seats")
    var seats : Int = 0
}