package org.edrodev.testmeep.model

import com.google.gson.annotations.SerializedName

class MopedResource : VehicleResource() {

    @SerializedName("helmets")
    var helmets: Int = 0
}