package org.edrodev.testmeep.model

import com.google.gson.annotations.SerializedName

open class VehicleResource : Resource() {

    @SerializedName("licencePlate")
    var licencePlate: String = ""

    @SerializedName("range")
    var range: Int = 0

    @SerializedName("batteryLevel")
    var batteryLevel: Int = 0

    @SerializedName("model")
    var model: String = ""
}